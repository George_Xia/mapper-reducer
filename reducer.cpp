#include <iostream>
#include <string>
 
using namespace std;
int main() { //reducer将会被封装成一个独立进程，因而需要有main函数
  string cur_key, last_key, value;
  cin >> cur_key >> value;
  last_key = cur_key;
  int n = 1;
  while(cin >> cur_key) { //读取map task输出结果
    cin >> value;
    if(last_key != cur_key) { //识别下一个key
      cout << last_key << "\t" << n << endl;
      last_key = cur_key;
      n = 1;
    } else { //获取key相同的所有value数目
      n++; //key值相同的，累计value值
    }   
  }
  cout << last_key << "\t" << n << endl;
  return 0;
}
