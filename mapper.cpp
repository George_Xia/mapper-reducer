#include <iostream>
#include <string>
using namespace std;
int main() {
  string key;
  while(cin >> key) {
    cout << key << "\t" << "1" << endl;
    // Define counter named counter_no in group counter_group
    cerr << "reporter:counter:counter_group,counter_no,1\n";
    // dispaly status
    cerr << "reporter:status:processing......\n";
    // Print logs for testing
    cerr << "This is log, will be printed in stdout file\n";
  }
  return 0;
}
